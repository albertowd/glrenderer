# GL Renderer

## A simple OpenGL obj rederer.

For now the program just draws a static gay triangle and rotates the camera. It uses OpenGL 3.2+ with shaders to draw objects.

## General Information

Hot keys:

    * c - toggle ortho/perspective cameras
    * f - toggle fullscreen
    * escape - exits the program

Cross-platform libs:

     * OpenGL 3.2+ - for objects drawing [link](https://www.opengl.org)
     * GLEW - necessary for OpenGL extensions (most important: shaders!) [link](http://glew.sourceforge.net)
     * GLM - great for vector and matrices, util for perspectives too [link](https://glm.g-truc.net)
     * SDL 2.0 - window and input management [link](https://www.libsdl.org)

## Supported Platforms

Event with use of only cross-platform libraries, I'm coding just on Linux. Windows and OSX support will be available soon.

| Platform | Supported |
| ---------|-----------|
| Linux    |     x     |
| OSX      |           |
| Windows  |           |

### On Linux

Instal required libraries before compile!
> sudo apt-get install libosmesa6-dev libglew-dev libglm-dev libsdl2-dev

Just use the make command and run the program:
> cd glrenderer
>
> make [debug|debug32|release|release32]
>
> cd bin
>
>./GLRenderer[d|32d|32]

### On OSX

Magic?

### On Windows

Kill me...