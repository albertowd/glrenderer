#include "App.hpp"

int main(const int argc, const char **args)
{
    App app;
    return app.run();
}