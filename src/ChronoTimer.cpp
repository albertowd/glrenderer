#include "ChronoTimer.hpp"

ChronoTimer::ChronoTimer() : start(std::chrono::high_resolution_clock::now())
{
}

std::chrono::duration<double> ChronoTimer::getDuration()
{
    return std::chrono::duration_cast<std::chrono::duration<double>>(std::chrono::high_resolution_clock::now() - start);
}

std::chrono::milliseconds ChronoTimer::getDurationMS()
{
    return std::chrono::duration_cast<std::chrono::milliseconds>(std::chrono::high_resolution_clock::now() - start);
}

std::chrono::nanoseconds ChronoTimer::getDurationNS()
{
    return std::chrono::duration_cast<std::chrono::nanoseconds>(std::chrono::high_resolution_clock::now() - start);
}

std::chrono::seconds ChronoTimer::getDurationS()
{
    return std::chrono::duration_cast<std::chrono::seconds>(std::chrono::high_resolution_clock::now() - start);
}

std::chrono::high_resolution_clock::time_point ChronoTimer::getStartPoint()
{
    return start;
}

void ChronoTimer::reset()
{
    start = std::chrono::high_resolution_clock::now();
}