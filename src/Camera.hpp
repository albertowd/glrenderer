#ifndef CAMERA_HPP
#define CAMERA_HPP

#include <glm/vec3.hpp>
#include <glm/mat4x4.hpp>

#define CAMERA_HD_RATIO 1280.0f / 720.0f
#define TO_RADIANS (float)M_PI / 180.0f

class Camera
{
public:
  Camera();
  Camera(const glm::vec3 position);

  float getAspectRatio() const;
  glm::vec3 getPosition() const;

  void addAspectRatio(const float addAspectRatio);
  void addPosition(const glm::vec3 addPosition);
  void setAspectRatio(const float aspectRatio);
  void setPosition(const glm::vec3 position);

  virtual glm::mat4 getMVP() const = 0;

protected:
  float aspectRatio;
  glm::vec3 position;
};

class CameraOrtho : public Camera
{
public:
  CameraOrtho();
  CameraOrtho(const glm::vec3 position);

  float getZoom() const;

  void addZoom(const float addZoom);
  void setZoom(const float zoom);

  virtual glm::mat4 getMVP() const;

private:
  float zoom;
};

class Camera3D : public Camera
{
public:
  Camera3D();

  float getFOV() const;
  float getzFar() const;
  float getzNear() const;
  glm::vec3 getLookAt() const;
  glm::vec3 getUp() const;

  void addFOV(const float addFOV);
  void addLookAt(const glm::vec3 addLookAt);
  void addUp(const glm::vec3 addUp);
  void addZFar(const float addZFar);
  void addZNear(const float addZNear);
  void setFOV(const float fov);
  void setLookAt(const glm::vec3 lookAt);
  void setUp(const glm::vec3 up);
  void setZFar(const float zFar);
  void setZNear(const float zNear);

  virtual glm::mat4 getMVP() const;

private:
  float fov;
  float zFar;
  float zNear;
  glm::vec3 lookAt;
  glm::vec3 up;
};

#endif