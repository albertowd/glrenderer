#ifndef APP_HPP
#define APP_HPP

#include <string>

#include <GL/glew.h>
#include <SDL2/SDL.h>
#include <SDL2/SDL_opengl.h>

#include "Camera.hpp"
#include "Model.hpp"

class App
{
public:
  App(const std::string title = "GL Renderer", const int height = 720, const int width = 1280);
  ~App();

  bool isValid();
  int run();

private:
  bool handleEvents();
  void render();
  void setUpGL();
  void update();

  bool isRunning;
  bool useOrtho;
  CameraOrtho cameraOrtho;
  Camera3D camera;
  Model *model;
  SDL_GLContext context;
  SDL_Window *window;
};

#endif