#include "Camera.hpp"

#include <cmath>
#include <glm/gtc/matrix_transform.hpp>

/************************************
 * Camera Methods
 ***********************************/

Camera::Camera() : Camera(glm::vec3(0.0f))
{
}

Camera::Camera(const glm::vec3 position) : aspectRatio(CAMERA_HD_RATIO), position(position)
{
}

glm::vec3 Camera::getPosition() const
{
    return position;
}

float Camera::getAspectRatio() const
{
    return aspectRatio;
}

void Camera::addAspectRatio(const float addAspectRatio)
{
    aspectRatio += addAspectRatio;
}

void Camera::addPosition(const glm::vec3 addPosition)
{
    position += addPosition;
}

void Camera::setAspectRatio(const float aspectRatio)
{
    this->aspectRatio = aspectRatio;
}

void Camera::setPosition(const glm::vec3 position)
{
    this->position = position;
}

/************************************
 * Camera Ortho Methods
 ***********************************/

CameraOrtho::CameraOrtho() : Camera(), zoom(1.0f)
{
}

CameraOrtho::CameraOrtho(const glm::vec3 position) : Camera(position), zoom(1.0f)
{
}

float CameraOrtho::getZoom() const
{
    return zoom;
}

void CameraOrtho::addZoom(const float addZoom)
{
    zoom += addZoom;
}

void CameraOrtho::setZoom(const float zoom)
{
    this->zoom = zoom;
}

glm::mat4 CameraOrtho::getMVP() const
{
    float halfAR(aspectRatio / 2.0f * zoom);
    return glm::ortho(-halfAR + position.x, halfAR + position.x, -zoom + position.y, zoom + position.y);
}

/************************************
 * Camera 3D Methods
 ***********************************/

Camera3D::Camera3D() : Camera(), fov(45.0f * M_PI / 180.0f), lookAt(0.0f), up(0.0f, 1.0f, 0.0f), zFar(100.0f), zNear(0.1f)
{
}

float Camera3D::getFOV() const
{
    return fov;
}

float Camera3D::getzFar() const
{
    return zFar;
}

float Camera3D::getzNear() const
{
    return zNear;
}

glm::vec3 Camera3D::getLookAt() const
{
    return lookAt;
}

glm::vec3 Camera3D::getUp() const
{
    return up;
}

void Camera3D::addFOV(const float addFOV)
{
    fov += addFOV;
}

void Camera3D::addLookAt(const glm::vec3 addLookAt)
{
    lookAt += addLookAt;
}

void Camera3D::addUp(const glm::vec3 addUp)
{
    up += addUp;
}

void Camera3D::addZFar(const float addZFar)
{
    zFar += addZFar;
}

void Camera3D::addZNear(const float addZNear)
{
    zNear += addZNear;
}

void Camera3D::setFOV(const float fov)
{
    this->fov = fov;
}

void Camera3D::setLookAt(const glm::vec3 lookAt)
{
    this->lookAt = lookAt;
}

void Camera3D::setUp(const glm::vec3 up)
{
    this->up = up;
}

void Camera3D::setZFar(const float zFar)
{
    this->zFar = zFar;
}

void Camera3D::setZNear(const float zNear)
{
    this->zNear = zNear;
}

glm::mat4 Camera3D::getMVP() const
{
    glm::mat4 m(glm::lookAt(position, lookAt, up));
    glm::mat4 p(glm::perspective(fov, aspectRatio, zNear, zFar));
    return p * m;
}