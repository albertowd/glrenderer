#include "Model.hpp"

#include <glm/vec3.hpp>
#include <glm/mat4x4.hpp>

#include "Util.hpp"

float colors[] = {
    1.0f, 0.0f, 0.0f,
    0.0f, 1.0f, 0.0f,
    0.0f, 0.0f, 1.0f};

float vertices[9] = {
    0.0f, 0.5f, 0.0f,
    0.5f, -0.5f, 0.0f,
    -0.5f, -0.5f, 0.0f};

float rotation[3] = {1.0f, 1.0f, 0.0f};

unsigned int indexes[3] = {0, 1, 2};

Model::Model() : fShader(0), id(0), mvpLocation(0), vao(0), vShader(0)
{
    glGenVertexArrays(1, &vao);
    glBindVertexArray(vao);
    glGenBuffers(3, bos);

    // First, store the vertices.
    glBindBuffer(GL_ARRAY_BUFFER, bos[0]);
    glBufferData(GL_ARRAY_BUFFER, 9 * sizeof(float), vertices, GL_STATIC_DRAW);
    // Specify that our coordinate data is going into attribute index 0, and contains two floats per vertex.
    glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 0, 0);
    glEnableVertexAttribArray(0);

    // Second, store the colors.
    glBindBuffer(GL_ARRAY_BUFFER, bos[1]);
    glBufferData(GL_ARRAY_BUFFER, 9 * sizeof(float), colors, GL_STATIC_DRAW);
    // Specify that our coordinate data is going into attribute index 0, and contains two floats per vertex.
    glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, 0, 0);
    glEnableVertexAttribArray(1);

    // Third, store the indexes.
    glBindBuffer(GL_ARRAY_BUFFER, bos[2]);
    glBufferData(GL_ARRAY_BUFFER, 3 * sizeof(float), indexes, GL_STATIC_DRAW);

    const char *vertexsource( util::fileToBuff("resources/vertex.glsl") );
    vShader = glCreateShader(GL_VERTEX_SHADER);
    glShaderSource(vShader, 1, (const GLchar **)&vertexsource, 0);
    glCompileShader(vShader);
    delete[] vertexsource;

    GLint isCompiled(true);
    glGetShaderiv(vShader, GL_COMPILE_STATUS, &isCompiled);
    if (!isCompiled)
        util::showShaderLog(vShader);

    const char *fragmentsource( util::fileToBuff("resources/fragment.glsl") );
    fShader = glCreateShader(GL_FRAGMENT_SHADER);
    glShaderSource(fShader, 1, (const GLchar **)&fragmentsource, 0);
    glCompileShader(fShader);
    delete[] fragmentsource;

    glGetShaderiv(fShader, GL_COMPILE_STATUS, &isCompiled);
    if (!isCompiled)
        util::showShaderLog(fShader);

    id = glCreateProgram();
    glAttachShader(id, vShader);
    glAttachShader(id, fShader);
    glBindAttribLocation(id, 0, "in_Position");
    glBindAttribLocation(id, 1, "in_Color");
    glLinkProgram(id);

    mvpLocation = glGetUniformLocation(id, "mvp");
    glGetProgramiv(id, GL_LINK_STATUS, &isCompiled);
    if (!isCompiled)
        util::showProgramLog(id);
}

Model::~Model()
{
    if (id > 0)
    {
        glDetachShader(id, fShader);
        glDetachShader(id, vShader);
        glDeleteBuffers(3, bos);
        glDeleteVertexArrays(1, &vao);
        glDeleteProgram(id);
    }
}

void Model::draw(const glm::mat4 mvp)
{
    if (id > 0)
    {
        glUseProgram(id);
        glBindVertexArray(vao);

        mvpLocation = glGetUniformLocation(id, "mvp");
        glUniformMatrix4fv(mvpLocation, 1, GL_FALSE, &mvp[0][0]);

        //glDrawArrays(GL_TRIANGLES, 0, 3);
        glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, bos[2]);
        glDrawElements(GL_TRIANGLES, 3, GL_UNSIGNED_INT, 0);
        glUseProgram(0);
    }
}