#ifndef UTIL_HPP
#define UTIL_HPP

#include <string>

#include <SDL2/SDL.h>
#include <GL/glew.h>

namespace util
{
char *fileToBuff(const std::string filePath);
void errorMessage(const std::string message, const std::string title = "App Error");
void infoMessage(const std::string message, const std::string title = "App Info");
void showProgramLog(const GLuint program);
void showShaderLog(const GLuint shader);
void warningMessage(const std::string message, const std::string title = "App Warning");
}

#endif