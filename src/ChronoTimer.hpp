#ifndef CHRONOTIMER_HPP
#define CHRONOTIMER_HPP

#include <chrono>

class ChronoTimer
{
public:
  ChronoTimer();

  std::chrono::duration<double> getDuration();
  std::chrono::milliseconds getDurationMS();
  std::chrono::nanoseconds getDurationNS();
  std::chrono::seconds getDurationS();
  std::chrono::high_resolution_clock::time_point getStartPoint();

  void reset();

private:
  std::chrono::high_resolution_clock::time_point start;
};

#endif