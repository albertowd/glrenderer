#include "Util.hpp"

char *util::fileToBuff(const std::string filePath)
{
    FILE *fptr;
    long length;
    char *buf;

    fptr = fopen(filePath.c_str(), "rb"); /* Open file for reading */
    if (!fptr)                            /* Return NULL on failure */
        return NULL;
    fseek(fptr, 0, SEEK_END);         /* Seek to the end of the file */
    length = ftell(fptr);             /* Find out how many bytes into the file we are */
    buf = (char *)malloc(length + 1); /* Allocate a buffer for the entire length of the file and a null terminator */
    fseek(fptr, 0, SEEK_SET);         /* Go back to the beginning of the file */
    fread(buf, length, 1, fptr);      /* Read the contents of the file in to the buffer */
    fclose(fptr);                     /* Close the file */
    buf[length] = 0;                  /* Null terminator */

    return buf; /* Return the buffer */
}

void util::errorMessage(const std::string message, const std::string title)
{
    SDL_ShowSimpleMessageBox(SDL_MESSAGEBOX_ERROR, title.c_str(), message.c_str(), nullptr);
}

void util::infoMessage(const std::string message, const std::string title)
{
    SDL_ShowSimpleMessageBox(SDL_MESSAGEBOX_INFORMATION, title.c_str(), message.c_str(), nullptr);
}

void util::showProgramLog(const GLuint program)
{
    //Make sure name is shader
    if (glIsProgram(program))
    {
        //Program log length
        int infoLogLength = 0;
        int maxLength = infoLogLength;

        //Get info string length
        glGetProgramiv(program, GL_INFO_LOG_LENGTH, &maxLength);

        //Allocate string
        char *infoLog = new char[maxLength];

        //Get info log
        glGetProgramInfoLog(program, maxLength, &infoLogLength, infoLog);
        if (infoLogLength > 0)
            util::warningMessage(std::string(infoLog), "GL Program Log");

        //Deallocate string
        delete[] infoLog;
    }
    else
        util::warningMessage(std::string("Invalid program: ") + std::to_string(program), "GL Program Log");
}

void util::showShaderLog(const GLuint shader)
{
    //Make sure name is shader
    if (glIsShader(shader))
    {
        //Shader log length
        int infoLogLength = 0;
        int maxLength = infoLogLength;

        //Get info string length
        glGetShaderiv(shader, GL_INFO_LOG_LENGTH, &maxLength);

        //Allocate string
        char *infoLog = new char[maxLength];

        //Get info log
        glGetShaderInfoLog(shader, maxLength, &infoLogLength, infoLog);
        if (infoLogLength > 0)
            util::warningMessage(std::string(infoLog), "GL Shader Log");

        //Deallocate string
        delete[] infoLog;
    }
    else
        util::warningMessage(std::string("Invalid shader: ") + std::to_string(shader), "GL Shader Log");
}

void util::warningMessage(const std::string message, const std::string title)
{
    SDL_ShowSimpleMessageBox(SDL_MESSAGEBOX_WARNING, title.c_str(), message.c_str(), nullptr);
}