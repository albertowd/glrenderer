#include "App.hpp"

#include <cmath>

#include "ChronoTimer.hpp"
#include "Util.hpp"

ChronoTimer dTimer;
const float rotationSpeed(90.0f * TO_RADIANS);

App::App(const std::string title, const int height, const int width) : context(nullptr), isRunning(false), model(nullptr), useOrtho(false), window(nullptr)
{
    if (SDL_Init(SDL_INIT_VIDEO) < 0)
        util::errorMessage(SDL_GetError(), "SDL could not initialize!");
    else if (!SDL_GL_SetAttribute(SDL_GL_CONTEXT_MAJOR_VERSION, 3) && !SDL_GL_SetAttribute(SDL_GL_CONTEXT_MINOR_VERSION, 2))
    {
        SDL_GL_SetAttribute(SDL_GL_DEPTH_SIZE, 24);
        SDL_GL_SetAttribute(SDL_GL_DOUBLEBUFFER, 1);
        SDL_GL_SetAttribute(SDL_GL_MULTISAMPLEBUFFERS, 1);
        SDL_GL_SetAttribute(SDL_GL_MULTISAMPLESAMPLES, 4);
        SDL_GL_SetSwapInterval(1);

        window = SDL_CreateWindow(title.c_str(), SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED, width, height, SDL_WINDOW_OPENGL | SDL_WINDOW_SHOWN);
        if (window != nullptr)
        {
            context = SDL_GL_CreateContext(window);
            if (context != nullptr)
            {
                // Global needed to use extensions.
                glewExperimental = GL_TRUE;
                GLenum glewError(glewInit());
                if (GLEW_OK != glewError)
                    util::warningMessage(reinterpret_cast<const char *>(glewGetErrorString(glewError)), "GLEW could not initialize!");
                this->setUpGL();
            }
            else
                util::errorMessage(SDL_GetError(), "GL Context 3.2 could not be created!");
        }
        else
            util::errorMessage(SDL_GetError(), "SDL Window could not be created!");
    }
    else
        util::errorMessage("GL 3.2 is not supported.");
}

App::~App()
{
    if (window)
    {
        delete model;
        model = nullptr;

        SDL_DestroyWindow(window);
        context = nullptr;
        window = nullptr;
    }

    SDL_Quit();
}

bool App::isValid()
{
    return context && window;
}

bool App::handleEvents()
{
    bool continueRunning(true);
    SDL_Event evt;
    while (SDL_PollEvent(&evt))
    {
        continueRunning = SDL_QUIT != evt.type;
        switch (evt.type)
        {
        case SDL_KEYUP:
            continueRunning &= SDLK_ESCAPE != evt.key.keysym.sym;
            switch (evt.key.keysym.sym)
            {
            case SDLK_c:
                useOrtho = !useOrtho;
                break;

            case SDLK_f:
                SDL_SetWindowFullscreen(window, SDL_GetWindowFlags(window) & SDL_WINDOW_FULLSCREEN ? 0 : SDL_WINDOW_FULLSCREEN);
                break;
            }
            break;
        }
    }
    return continueRunning;
}

int App::run()
{
    dTimer.reset();
    isRunning = this->isValid();

    while (isRunning)
    {
        isRunning = this->handleEvents();
        this->update();
        this->render();
    }

    return 0;
}

void App::setUpGL()
{
    glClearColor(0.5, 0.5, 0.5, 1.0);
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
    glDepthFunc(GL_LESS);
    glDisable(GL_CULL_FACE);
    glEnable(GL_BLEND);
    glEnable(GL_DEPTH_TEST);

    int width, height;
    SDL_GetWindowSize(window, &width, &height);
    glViewport(0, 0, width, height);

    camera.setAspectRatio(width / (float)height);
    cameraOrtho.setAspectRatio(width / (float)height);
    model = new Model();
}

void App::render()
{
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    model->draw(useOrtho ? cameraOrtho.getMVP() : camera.getMVP());
    SDL_GL_SwapWindow(window);
}

void App::update()
{
    glm::vec3 position(0.5f, 0.5f, 0.0f);
    glm::vec3 position3D(2.0f, 0.0f, 2.0f);

    float dRotation((float)fmod(dTimer.getDuration().count(), 4.0));
    dRotation *= rotationSpeed;

    position.x *= sinf(dRotation);
    position.y *= cosf(dRotation);
    position3D.x *= sinf(dRotation);
    position3D.z *= cosf(dRotation);
    camera.setPosition(position3D);
    cameraOrtho.setPosition(position);
}