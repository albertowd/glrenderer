#ifndef MODEL_HPP
#define MODEL_HPP

#include <GL/glew.h>
#include <glm/mat4x4.hpp>

class Model
{
public:
  Model();
  ~Model();

  void draw(const glm::mat4 mvp);

private:
  GLint mvpLocation;
  GLuint bos[3];
  GLuint fShader;
  GLuint id;
  GLuint vao;
  GLuint vShader;
};

#endif