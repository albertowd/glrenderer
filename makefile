# CPPS Files to compile.
CPPS = src/*

# CC Compilerto use.
CC = g++

# COMPILER_FLAGS Compiler aditional options.
# -g Generate debug symbols.
# -m32/-m64 Generate x86/x64 binaries.
# -std=c++14 C++14 standards.
# -w Supress warnings
COMPILER_FLAGS = -std=c++14

# LINKER_FLAGS Libraries to link
LINKER_FLAGS = -lGL -lGLEW -lSDL2

# OBJ Binary name
OBJ = GLRenderer

# OUT_DIR The binary/resources output directory
OUT_DIR = bin/

# RES_DIR Resources directory.
RES_DIR = resources/

# Default release x64 compilation.
release:
	$(CC) $(CPPS) -w -m64 $(COMPILER_FLAGS) $(LINKER_FLAGS) -o $(OUT_DIR)$(OBJ)
	cp -r $(RES_DIR) $(OUT_DIR)$(RES_DIR)

# Cleans the output directory.
clean:
	rm $(OUT_DIR)$(OBJ)*
	rm -r $(OUT_DIR)$(RES_DIR)

# Debug x64 compilation.
debug:
	$(CC) $(CPPS) -g -m64 $(COMPILER_FLAGS) $(LINKER_FLAGS) -o $(OUT_DIR)$(OBJ)d
	cp -r $(RES_DIR) $(OUT_DIR)$(RES_DIR)

# Debug x86 compilation.
debug32:
	$(CC) $(CPPS) -g -m32 $(COMPILER_FLAGS) $(LINKER_FLAGS) -o $(OUT_DIR)$(OBJ)32d
	cp -r $(RES_DIR) $(OUT_DIR)$(RES_DIR)

# Release x86 compilation.
release32:
	$(CC) $(CPPS) -w -m32 $(COMPILER_FLAGS) $(LINKER_FLAGS) -o $(OUT_DIR)$(OBJ)32
	cp -r $(RES_DIR) $(OUT_DIR)$(RES_DIR)