#version 150

uniform mat4 mvp;

in vec3 in_Position;
in vec3 in_Color;

out vec3 f_Color;

void main(void) {
    gl_Position = mvp * vec4(in_Position, 1.0);
    f_Color = in_Color;
}