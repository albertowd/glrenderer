#version 150

// It was expressed that some drivers required this next line to function properly
precision highp float;

in vec3 f_Color;

void main(void) {
    gl_FragColor = vec4(f_Color, 1.0);
}